<?php

/** 移除物件空直資料 */
if (!function_exists('randomData')) {

    function randomTest($questions)
    {
        $randomDatas = [];
        $ids         = [];
        for ($i = 0; $i < $questions->count(); $i++) {
            $randomDatas[] = $randomData = $questions->whereNotIn('id', $ids)->random(1)->first();
            $ids[]         = $randomData->id;
        }
        return $randomDatas;
    }
}


