<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Admin extends Controller
{
    function __construct(Users $users)
    {
        $this->users = $users;
    }

    function index()
    {
        $is_login = session('is_login', false);
        if (!$is_login) {
            return redirect()->route('admin-loginPage');
        }

        $users = $this->users->get();

        $data = [
            'users' => $users,
        ];

        return view('admin.home', $data);
    }

    function testContent($userId)
    {
        $測驗資訊   = DB::select("select a.name 姓名,c.name 測驗題目,b.strat_time 作答開始時間,b.end_time 作答結束時間,c.limit_time 限制時間,timediff(  b.end_time,b.strat_time) 花費作答時間 
                                from users a, user_test b ,test c
                                where a.id = b.user_id
                                and c.id = b.test_id
                                and a.id not in(1,2)
                                and a.id = {$userId} 
                                order by a.name;");

        $邏輯測驗分數 = DB::select("select a.name 姓名,c.name 測驗名稱,sum(d.point) 分數 
                        from users a,user_test b ,test c,answer d
                        where a.id = b.user_id 
                        and c.id = b.test_id
                        and a.id not in(1,2)
                        and c.id = 1
                        and d.user_test_id = b.id
                        and d.is_check = 1
                        and a.id = {$userId} 
                        group by a.name,c.name
                        order by a.name,c.id;");
        $性向測驗   = DB::select("select a.name 姓名,c.name,f.type 類別,count(f.type) 數量,
                        case f.type
                        when 1 then '老虎-支配型'
                        when 2 then '孔雀-社交型'
                        when 3 then '無尾熊-安定型'
                        when 4 then '貓頭鷹-分析型'
                        end '型態'
                        from users a,user_test b ,test c,answer d,question e,question_option f
                        where a.id = b.user_id 
                        and c.id = b.test_id
                        and a.id not in(1,2)
                        and c.id = 2
                        and d.answer = f.id
                        and d.user_test_id = b.id
                        and d.question_id = e.id
                        and e.id = f.question_id
                        and a.id = {$userId}
                        group by a.name,c.name,f.type
                        order by a.name,f.type;");
        $程式實作分數 = DB::select("select a.name 姓名,c.name 測驗名稱,sum(d.point) 分數,count(d.id) 做題數 
                        from users a,user_test b ,test c,answer d
                        where a.id = b.user_id 
                        and c.id = b.test_id
                        and a.id not in(1,2)
                        and c.id = 3
                        and d.user_test_id = b.id
                        and d.is_check = 1
                        and a.id = {$userId} 
                        group by a.name,c.name
                        order by a.name,c.id;");


        $data = [
            "測驗資訊"=>$測驗資訊,
            "邏輯測驗分數"=>$邏輯測驗分數,
            "性向測驗"=>$性向測驗,
            "程式實作分數"=>$程式實作分數,
        ];
        return view('admin.content', $data);
    }

    function login(Request $request)
    {
        $password     = $request->input('password');
        $truePassword = env('password', '1234qwer');


        if ($password == $truePassword) {
            session(["is_login" => true]);
        }
        return redirect()->route('admin-home');
    }

}
