<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\question;
use App\Models\QuestionOption;
use App\Models\Test;
use App\Models\UserTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class Home extends Controller
{

    private $test;
    private $question;
    private $questionOption;
    private $userTest;
    private $answer;

    function __construct(
        Test $test,
        Question $question,
        QuestionOption $questionOption,
        UserTest $userTest,
        Answer $answer
    ) {

        $this->test           = $test;
        $this->question       = $question;
        $this->questionOption = $questionOption;
        $this->userTest       = $userTest;
        $this->answer         = $answer;

    }

    /**
     * 後台首頁
     */
    function home()
    {
        return view('backend.index');
    }

    /**
     * 測試說明頁面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function test(Request $request)
    {
        $testId   = $request->id;
        $userId   = Auth::user()->getAuthIdentifier();
        $userTest = $this->userTest->where('test_id', $testId)->where('user_id', $userId)->get();
        if ($userTest->count() > 0) {
            $endTime = $userTest->first()->end_time;
            if (!is_null($endTime)) {
                return view("backend.notice", ["msg" => "測驗已完成"]);
            }
        }
        $test = $this->test->find($testId);

        $data = [
            "test" => $test,
        ];
        return view('backend.test', $data);
    }

    /**
     * 開始作答
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function doTest(Request $request)
    {
        $testId       = $request->id;
        $test         = $this->test->find($testId);
        $userId       = Auth::user()->getAuthIdentifier();
        $elapsedSec   = 0;  //剩餘時間
        $limitTime    = $test->limit_time;  //考試限制間(分)
        $limitTimeSec = $limitTime * 60;
        //確認是否有答案相關資料
        $userTest = $this->userTest->where('user_id', $userId)->where('test_id', $testId)->get();
        if ($userTest->count() != 0) {//有-取開始作答時間算出剩下可作答時間
            $userTest    = $userTest->first();
            $stratTime   = $userTest->strat_time;
            $endTime     = $userTest->end_time;
            $userTestId  = $userTest->id;
            $elapsedSec  = strtotime(date("Y-m-d H:i:s")) - strtotime($stratTime);
            $timeLeftSec = $limitTimeSec - $elapsedSec;
            if ($timeLeftSec <= 0 || !is_null($endTime)) {
                return view("backend.notice", ["msg" => "測驗結束"]);
            }

        } else {//無-新增答案相關資料並開始計時
            $userTest   = UserTest::create([
                "user_id"    => $userId,
                "test_id"    => $testId,
                "strat_time" => date('Y-m-d H:i:s'),
            ]);
            $userTestId = $userTest->id;
        }


        $questions = $this->question->where('test_id', $testId)->get();
        $questions = randomTest($questions);

        $data = [
            "userTestId"   => $userTestId,
            "questions"    => $questions,
            "elapsedSec"   => $elapsedSec,
            "limitTimeSec" => $limitTimeSec,
            "test"=>$test,
        ];
        return view('backend.doTest', $data);
    }

    /**
     * 寫入答案
     */
    function answer(Request $request)
    {
        logger($request->all());
        $userTestId = $request->input('userTestId');
        $answers    = $request->input('answers',[]);

        $userId   = Auth::user()->getAuthIdentifier();
        $userTest = $this->userTest->find($userTestId);
        if ($userTest->user_id != $userId) {
            logger("對應答案人員錯誤，使用者ID{$userId}");
            return view("backend.notice", ["msg" => "錯誤，請通知面試聯絡人員"]);
        }

        $testId       = $userTest->test_id;
        $test         = $this->test->find($testId);
        $limitTime    = $test->limit_time;
        $limitTimeSec = $limitTime * 60;
        $stratTime    = $userTest->strat_time;
        $endTime      = $userTest->end_time;
        $elapsedSec   = strtotime(date("Y-m-d H:i:s")) - strtotime($stratTime);
        $timeLeftSec  = $limitTimeSec - $elapsedSec + 60; //給一分鐘的緩衝
        if (!is_null($endTime)) {
            logger($endTime);
            return view("backend.notice", ["msg" => "已提交過答案"]);
        }
        if ($timeLeftSec <= 0) {
            return view("backend.notice", ["msg" => "超過作答時間"]);
        }

        //清空答案重給
        $this->answer->where('user_test_id', $userTestId)->delete();
        //壓作答時間
        $this->userTest->where('id', $userTestId)->update(['end_time' => date('Y-m-d H:i:s')]);

        //組新增的array

        //新增答案
        foreach ($answers as $questionId => $answer) {
            $point        = 0;
            $question     = $this->question->find($questionId);
            $questionType = $question->question_type;
            if ($questionType == 1) {
                $questionOption = $this->questionOption->find($answer);
                if ($questionOption->is_answer == 1) {
                    $point = $question->point;
                }
            }

            $data = [
                "user_test_id" => $userTestId,
                "question_id"  => $questionId,
                "answer"       => (is_null($answer)) ? "未填寫" : $answer,
                "point"        => $point,
                "is_check"     => ($questionType == 1) ? 1 : 0,
            ];
            Answer::create($data);
        }
        return view("backend.notice", ["msg" => "測試完成"]);


    }

    /**
     * 確認作答時間
     */
    function checkTime(Request $request)
    {
        $result = (object)[
            'code' => -1
        ];

        $userTestId = $request->input('userTestId');

        $userTest     = $this->userTest->find($userTestId);
        $testId       = $userTest->test_id;
        $test         = $this->test->find($testId);
        $limitTime    = $test->limit_time;
        $limitTimeSec = $limitTime * 60;

        $stratTime   = $userTest->strat_time;
        $endTime     = $userTest->end_time;
        $elapsedSec  = strtotime(date("Y-m-d H:i:s")) - strtotime($stratTime);
        $timeLeftSec = $limitTimeSec - $elapsedSec; //給一分鐘的緩衝
        if (!is_null($endTime)) {
            return -1;
        }
        if ($timeLeftSec > 0) {
            $result->code        = 0;
            $result->timeLeftSec = $timeLeftSec;
        }


        return response()->json($result);
    }


}
