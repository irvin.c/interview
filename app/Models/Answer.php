<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table    = 'answer';
    protected $fillable = [
        "user_test_id",
        "question_id",
        "answer",
        "point",
        "is_check",
    ];
}
