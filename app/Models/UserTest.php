<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = "user_test";

    protected $fillable = [
        "user_id","test_id","strat_time"
    ];

}
