<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $table = "question";

    function option(){

        return $this->hasMany('App\Models\QuestionOption', 'question_id',"id");
    }

}
