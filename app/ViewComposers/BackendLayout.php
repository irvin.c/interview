<?php

namespace App\Http\ViewComposers;


use App\Models\Test;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

class BackendLayout
{
    function compose(View $view){
        $test = App::make(Test::class);
        $view->with('sidenav', $test->get());
    }
}
