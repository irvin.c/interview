<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_option', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('question_id')->comment('所屬題目ID');
            $table->string('description')->comment('選項描述');
            $table->integer('is_answer')->comment('是否答案 0.否 1.是');
            $table->string('type')->nullable()->comment('選項類別,信向測驗使用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_option');
    }
}
