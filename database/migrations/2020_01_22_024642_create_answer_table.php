<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_test_id')->comment('所屬使用者id');
            $table->integer('question_id')->comment('所屬題目id');
            $table->text('answer')->comment('答案');
            $table->integer('point')->comment('分數');
            $table->string('type')->nullable()->comment('答案類別,信向測驗使用');
            $table->integer('is_check')->comment('是否評分 0.否 1.是');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer');

    }
}
