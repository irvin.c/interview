<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_test', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('test_id')->comment('測試項目所屬id');
            $table->integer('user_id')->comment('所屬使用者id');
            $table->dateTime('strat_time')->comment('開始測驗時間');
            $table->dateTime('end_time')->nullable()->comment('結束測驗時間');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_test');
    }
}
