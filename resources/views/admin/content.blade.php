<table class="gridtable">
    <tr>
        <th>姓名</th>
        <th>測驗題目</th>
        <th>作答開始時間</th>
        <th>作答結束時間</th>
        <th>限制時間</th>
        <th>花費作答時間</th>
    </tr>
    @foreach($測驗資訊 as $測驗資訊資料)
        <tr>
            <td>{{$測驗資訊資料->姓名}}</td>
            <td>{{$測驗資訊資料->測驗題目}}</td>
            <td>{{$測驗資訊資料->作答開始時間}}</td>
            <td>{{$測驗資訊資料->作答結束時間}}</td>
            <td>{{$測驗資訊資料->限制時間}}</td>
            <td>{{$測驗資訊資料->花費作答時間}}</td>
        </tr>
    @endforeach

</table>
<table class="gridtable">
    <tr>
        <th>姓名</th>
        <th>測驗名稱</th>
        <th>分數</th>

    </tr>
    @foreach($邏輯測驗分數 as $邏輯測驗分數資料)
        <tr>
            <td>{{$邏輯測驗分數資料->姓名}}</td>
            <td>{{$邏輯測驗分數資料->測驗名稱}}</td>
            <td>{{$邏輯測驗分數資料->分數}}</td>
        </tr>
    @endforeach

</table>
<table class="gridtable">
    <tr>
        <th>姓名</th>
        <th>類別</th>
        <th>數量</th>
        <th>型態</th>

    </tr>
    @foreach($性向測驗 as $性向測驗資料)
        <tr>
            <td>{{$性向測驗資料->姓名}}</td>
            <td>{{$性向測驗資料->類別}}</td>
            <td>{{$性向測驗資料->數量}}</td>
            <td>{{$性向測驗資料->型態}}</td>
        </tr>
    @endforeach

</table>

<table class="gridtable">
    <tr>
        <th>姓名</th>
        <th>測驗名稱</th>
        <th>分數</th>
        <th>做題數</th>

    </tr>
    @foreach($程式實作分數 as $程式實作分數資料)
        <tr>
            <td>{{$程式實作分數資料->姓名}}</td>
            <td>{{$程式實作分數資料->測驗名稱}}</td>
            <td>{{$程式實作分數資料->分數}}</td>
            <td>{{$程式實作分數資料->做題數}}</td>
        </tr>
    @endforeach

</table>

<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
    table.gridtable {
        font-family: verdana,arial,sans-serif;
        font-size:11px;
        color:#333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }
    table.gridtable th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }
    table.gridtable td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
    }
</style>












{{--$程式實作分數--}}