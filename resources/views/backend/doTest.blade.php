@extends('backend.layouts.index')
@section('content')
    <style type="text/css">
        textarea {
            resize: none;
        }
    </style>
    <h1>{{$test->name}}</h1>
    <div class="col-lg-12">
        <form id="answer" action="{{route('answer')}}" method="post">
            @csrf
            <input type="hidden" name="userTestId" value="{{$userTestId}}">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">題目</th>
                    <th scope="col">答案</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp

                @foreach($questions as $question)
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>
                            {!! $question->description !!}
                            @if($question->point != 0)
                                分數({{$question->point}}分)
                            @endif
                            <br>
                        </td>
                        <td>
                            @if($question->question_type == 1 || $question->question_type == 3)
                                @php
                                    $options = randomTest($question->option)
                                @endphp
                                @foreach($options  as $option)


                                    <input type="radio" name="answers[{{$question->id}}]"
                                           value="{{$option->id}}"> {{$option->description}}<br>
                                @endforeach

                            @elseif($question->question_type==2)
                                <textarea name="answers[{{$question->id}}]"
                                          style="width: 500px"  onkeyup="autogrow(this);"></textarea>
                            @endif
                        </td>
                    </tr>
                    @php
                        $i ++;
                    @endphp
                @endforeach

                </tbody>
                <tr>
                    <td colspan="3">
                        <input type="submit" class="btn btn-primary" value="完成">
                    </td>

                </tr>

            </table>
        </form>
    </div>

@endsection
@section('script')

    <script>
        $(document).ready(function () {
            setInterval("checkTime()", 7000);
            setInterval("Check_Time()", 1000);
            // $("textarea.auto-height").css("overflow", "hidden").bind("keydown keyup", function() {
            //     $(this).height('0px').height($(this).prop("scrollHeight") + 'px');
            // }).keydown();
        });
        function autogrow(textarea){
            var adjustedHeight=textarea.clientHeight;

            adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
            if (adjustedHeight>textarea.clientHeight){
                textarea.style.height=adjustedHeight+'px';
            }

        }

        function checkTime() {
            $.ajax({
                url: "{{route("checkTime")}}",
                type: "get",
                data: {
                    userTestId:{{$userTestId}}
                },
                dataType: "json",
                success: function (response) {

                    if (response.code !== 0) {
                        $("#answer").submit();
                    }
                },
                error: function () {
                }
            });
        }

        var SetMinute = {{$elapsedSec}};

        function Check_Time() {
            SetMinute += 1;
            var Check_i = document.getElementById("Check_i");

            var Cal_Hour = Math.floor(SetMinute / 3600);
            var Cal_Minute = Math.floor(Math.floor(SetMinute % 3600) / 60);
            var Cal_Second = SetMinute % 60;

            Check_i.innerHTML = Cal_Hour + "小時" + Cal_Minute + "分" + Cal_Second + "秒";
            if (SetMinute == {{$limitTimeSec}}) {
                $("#answer").submit();
            }

        }

    </script>
@endsection
@section('checTime')
    <span id="Check_Txt" style="color:#fffbfd">作答時間：<span id="Check_i">0小時0分0秒</span></span>
@endsection
