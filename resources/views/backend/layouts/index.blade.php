<!DOCTYPE html>
<html lang="en">
@include('backend.layouts.head')
<body class="sb-nav-fixed">
@include('backend.layouts.nav')
<div id="layoutSidenav">
    @include('backend.layouts.layoutSidenavNav')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                @yield('content')
            </div>
        </main>
    </div>


</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset("backend/js/scripts.js")}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="{{asset("backend/assets/demo/chart-area-demo.js")}}"></script>
<script src="{{asset("backend/assets/demo/chart-bar-demo.js")}}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="{{asset("backend/assets/demo/datatables-demo.js")}}"></script>
@yield('script')
</body>
</html>
