@extends('backend.layouts.index')
@section('content')

    <h1>邏輯測驗</h1>
    <div class="col-lg-6">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">注意事項</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{$test->description}}
                </td>
            </tr>
            <tr>
                <td>
                    測驗時間:{{$test->limit_time}}分鐘
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="btn btn-primary" value="開始測驗" onclick="doTest()">
                </td>
            </tr>

        </tbody>
    </table>
    </div>
    <script>
        function doTest() {
            window.location.href="{{route("doTest",[$test->id])}}";
        }
    </script>
@endsection