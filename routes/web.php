<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "Home@home")->middleware('auth')->name('index');
/** 考試頁面 */
Route::get('/test/{id}', "Home@test")->middleware('auth')->name('test');
/** 作答頁面 */
Route::get('/do/test/{id}', "Home@doTest")->middleware('auth')->name('doTest');
/** 送出答案 */
Route::post('/answer', "Home@answer")->middleware('auth')->name('answer');
/** 檢查作答時間 */
Route::get('/check/time',"Home@checkTime")->middleware('auth')->name('checkTime');
/** 首頁 */
Route::get('/home', "Home@home")->middleware('auth')->name('home');;

/** 登入頁面 */
Route::get('/admin/login', function (){
    return view('admin.login');
})->name('admin-loginPage');

/** 執行登入 */
Route::post('/admin/login','Admin@login' )->name('admin-login');

/** 首頁 */
Route::get('/admin/','Admin@index' )->name('admin-home');
/** 分數 */
Route::get('/admin/test/content/{userId}','Admin@testContent')->name('test-content');








Auth::routes();


